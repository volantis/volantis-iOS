//
//  ViewController.swift
//  Volantis
//
//  Created by Vincent von Rotz on 03/10/15.
//  Copyright © 2015 Volantis. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet var webView: UIWebView!
    
    let delegate = UIApplication.sharedApplication().delegate as! AppDelegate
    
    var url = "http://volantis.bimmler.ch:5000/"
    
    func loadAddressURL() {
        let requestURL = NSURL(string:url)
        let request = NSURLRequest(URL: requestURL!)
        webView.loadRequest(request)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        url += delegate.myID
        loadAddressURL()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

