//
//  AppDelegate.swift
//  Volantis
//
//  Created by Vincent von Rotz on 03/10/15.
//  Copyright © 2015 Volantis. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, PPKControllerDelegate, ESTBeaconManagerDelegate {

    var window: UIWindow?
    var myID: String!
    let beaconManager = ESTBeaconManager()


    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        PPKController.enableWithConfiguration("eyJzaWduYXR1cmUiOiJCMmRMQkVpMTQ5MHJnaW00OW9zR1BWM2VJOUY1SE5qL09heHVKTjg0WktFQ2dTTDN5S3pRZHhGZktBWG9rTUQ0STU2WmlxeGlZbC9ob1RmWVV2NEd2WFhLUEhxWFJIbkVwQjVkTWRTWVZNQzliak9BV2dHVnZ6RkNTdHVnTGpEN29PclFPR1Q3dUxMMXlwOWY1aGtMai9qQ2tSK1ZSQU9SMGhkanVuRy9FSFk9IiwiYXBwSWQiOjEyOTEsInZhbGlkVW50aWwiOjE2ODAwLCJhcHBVVVVJRCI6IkM0Qjg5RkE3LUMwN0UtNEU3Ni05ODhGLUJFQkQ1MjZGRDNBMiJ9", observer: self)
        myID = PPKController.myPeerID()
        application.statusBarHidden = true
        self.beaconManager.delegate = self
        self.beaconManager.requestAlwaysAuthorization()
        self.beaconManager.startMonitoringForRegion(CLBeaconRegion(
            proximityUUID: NSUUID(UUIDString: "B9407F30-F5F8-466E-AFF9-25556B57FE6D")!,
            major: 40236, minor: 26448, identifier: "monitored region"))
        UIApplication.sharedApplication().registerUserNotificationSettings(
            UIUserNotificationSettings(forTypes: .Alert, categories: nil))
        return true
    }

    func applicationWillResignActive(application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    func p2pPeerDiscovered(peer: PPKPeer!) {
        if let discoveryInfoString = NSString(data: peer.discoveryInfo, encoding: NSUTF8StringEncoding){
            if discoveryInfoString=="Valora"{
                NSLog("%@ is also a customer", peer.peerID)
                //let dict: NSDictionary = ["id": myID, "discovery_id": peer.peerID]
                //uploadNewMeeting(dict)
            }
            else{
                NSLog("No dice")
            }
        }
        else if let _ = peer.discoveryInfo{
            NSLog("No dice")//"%@ is here with discovery info: %@", peer.peerID, String(discoveryInfo))
        }
        
    }
    
    func p2pPeerLost(peer: PPKPeer!) {
        NSLog("%@ is no longer here", peer.peerID)
    }
    
    func didUpdateP2PDiscoveryInfoForPeer(peer: PPKPeer!) {
        if let discoveryInfoString = NSString(data: peer.discoveryInfo, encoding: NSUTF8StringEncoding){
            NSLog("%@ has updated discovery info: %@", peer.peerID, discoveryInfoString)
        }
        else if let discoveryInfo = peer.discoveryInfo
        {
            NSLog("%@ has updated discovery info: %@", peer.peerID, String(discoveryInfo))
        }
        else{
            NSLog("%@ changed some info, unable to decypher", peer.peerID)
        } 
    }
    
    func PPKControllerInitialized() {
        let myDiscoveryInfo = "Valora".dataUsingEncoding(NSUTF8StringEncoding)
        PPKController.startP2PDiscoveryWithDiscoveryInfo(myDiscoveryInfo)
    }

    func uploadNewMeeting(dict: NSDictionary){
        do {
            let request_json = try NSJSONSerialization.dataWithJSONObject(dict, options: NSJSONWritingOptions(rawValue: 0));
            let request = NSMutableURLRequest(URL: NSURL(string: "http://volantis.bimmler.ch:5000/discovery")!)
            request.HTTPMethod = "POST"
            request.HTTPBody = request_json
            let task = NSURLSession.sharedSession().dataTaskWithRequest(request) {
                data, response, error in
                
                if error != nil {
                    print("error=\(error)")
                    return
                }
                
                print("response = \(response)")
                
                let responseString = NSString(data: data!, encoding: NSUTF8StringEncoding)
                print("responseString = \(responseString)")
            }
            task.resume()
        } catch {
            NSLog("Totally dieded")
        }
    }
    
    func beaconManager(manager: AnyObject!, didEnterRegion region: CLBeaconRegion!) {
        let notification = UILocalNotification()
        notification.alertBody =
            "There's a K kiosk near you :]. " +
            "Might be worth it to check it out, " +
            "aren't you hungry?"
        UIApplication.sharedApplication().presentLocalNotificationNow(notification)
        //let dict: NSDictionary = ["id": myID, "discovery_id": ("40236" + "26448")]
        //uploadNewMeeting(dict)
    }
}

